package com.raphanum.tinkoffnews.constants

object Screens {
    const val NEWS_LIST_SCREEN = "newsListScreen"
    const val NEWS_DETAILS_SCREEN = "newsDetailsScreen"
}