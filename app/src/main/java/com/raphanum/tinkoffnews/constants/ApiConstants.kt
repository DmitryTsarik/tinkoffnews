package com.raphanum.tinkoffnews.constants

object ApiConstants {
    const val BASE_URL = "https://api.tinkoff.ru"
    const val GET_NEWS_LIST = "/v1/news"
    const val GET_NEWS_DETAILS = "/v1/news_content"
}