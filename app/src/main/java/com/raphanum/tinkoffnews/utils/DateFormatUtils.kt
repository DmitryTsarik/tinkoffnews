package com.raphanum.tinkoffnews.utils

import android.text.format.DateFormat

object DateFormatUtils {
    fun getFormattedDate(milliseconds: Long): String {
        return DateFormat.format("dd.MM.yyyy hh:mm", milliseconds).toString()
    }
}