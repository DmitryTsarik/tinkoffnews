package com.raphanum.tinkoffnews.di

import com.raphanum.tinkoffnews.presentation.presenters.NewsDetailsPresenter
import com.raphanum.tinkoffnews.presentation.presenters.NewsListPresenter
import com.raphanum.tinkoffnews.ui.activities.BaseActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AppModule::class, NavigationModule::class))
interface AppComponent {
    fun inject(activity: BaseActivity)

    fun getNewsListPresenter(): NewsListPresenter

    fun getNewsDetailsPresenter(): NewsDetailsPresenter
}