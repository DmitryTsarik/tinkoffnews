package com.raphanum.tinkoffnews.di

import android.app.Application
import android.arch.persistence.room.Room
import android.content.Context
import com.raphanum.tinkoffnews.data.api.TinkoffNewsApi
import com.raphanum.tinkoffnews.data.db.AppDatabase
import com.raphanum.tinkoffnews.data.repository.NewsLocalRepository
import com.raphanum.tinkoffnews.data.repository.NewsRepositoryImpl
import com.raphanum.tinkoffnews.data.repository.NewsRestRepository
import com.raphanum.tinkoffnews.domain.NewsInteractor
import com.raphanum.tinkoffnews.domain.NewsRepository
import com.raphanum.tinkoffnews.presentation.presenters.NewsDetailsPresenter
import com.raphanum.tinkoffnews.presentation.presenters.NewsListPresenter
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router
import javax.inject.Singleton


@Module
class AppModule(private val app: Application) {

    @Provides
    @Singleton
    fun provideApplicationContext() = app.applicationContext

    @Provides
    @Singleton
    fun provideApi() = TinkoffNewsApi.create()

    @Provides
    @Singleton
    fun provideNewsRestRepository(api: TinkoffNewsApi) = NewsRestRepository(api)

    @Provides
    @Singleton
    fun provideDatabase(context: Context): AppDatabase {
        return Room.databaseBuilder(
            context,
            AppDatabase::class.java, "database"
        ).build()
    }

    @Provides
    @Singleton
    fun provideLocalRepository(db: AppDatabase): NewsLocalRepository {
        return NewsLocalRepository(db)
    }

    @Provides
    @Singleton
    fun provideNewsRepository(
        restRepository: NewsRestRepository,
        localRepository: NewsLocalRepository
    ): NewsRepository =
        NewsRepositoryImpl(restRepository, localRepository)

    @Provides
    @Singleton
    fun provideNewsInteractor(repository: NewsRepository) = NewsInteractor(repository)

    @Provides
    @Singleton
    fun provideNewsListPresenter(interactor: NewsInteractor, router: Router) =
        NewsListPresenter(interactor, router)

    // TODO: подумать насчёт переноса в PerFragment scope
    @Provides
    @Singleton
    fun provideNewsDetailsPresenter(interactor: NewsInteractor, router: Router) =
        NewsDetailsPresenter(interactor, router)
}