package com.raphanum.tinkoffnews.ui.adapters

import android.support.v7.widget.RecyclerView
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.raphanum.tinkoffnews.R
import com.raphanum.tinkoffnews.domain.models.NewsItem
import com.raphanum.tinkoffnews.utils.DateFormatUtils
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.news_list_item.*

class NewsListAdapter(
    private var items: List<NewsItem>,
    private val listener: OnNewsItemClickListener
) : RecyclerView.Adapter<NewsListAdapter.NewsItemViewHolder>() {

    interface OnNewsItemClickListener {
        fun onClick(id: String)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): NewsItemViewHolder {
        val view: View =
                LayoutInflater.from(parent?.context).inflate(R.layout.news_list_item, parent, false)
        return NewsItemViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: NewsItemViewHolder?, position: Int) {
        holder?.bind(items[position])
    }

    fun setItems(items: List<NewsItem>) {
        this.items = items
        notifyDataSetChanged()
    }

    inner class NewsItemViewHolder(override val containerView: View?) :
            RecyclerView.ViewHolder(containerView), LayoutContainer {

        fun bind(model: NewsItem) {
            title.text = Html.fromHtml(model.title)
            date.text = DateFormatUtils.getFormattedDate(model.publicationDate)
            itemView.setOnClickListener({ listener.onClick(model.id) })
        }
    }
}