package com.raphanum.tinkoffnews.ui.fragments

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.raphanum.tinkoffnews.App
import com.raphanum.tinkoffnews.R
import com.raphanum.tinkoffnews.domain.models.NewsItem
import com.raphanum.tinkoffnews.presentation.presenters.NewsListPresenter
import com.raphanum.tinkoffnews.presentation.views.NewsListView
import com.raphanum.tinkoffnews.ui.adapters.NewsListAdapter
import kotlinx.android.synthetic.main.fragment_news_list.*


class NewsListFragment : MvpAppCompatFragment(),
        NewsListView {
    lateinit var adapter: NewsListAdapter

    @InjectPresenter
    lateinit var presenter: NewsListPresenter

    @ProvidePresenter
    fun initPresenter() = App.appComponent.getNewsListPresenter()

    override fun onCreateView(
        inflater: LayoutInflater?, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater!!.inflate(R.layout.fragment_news_list, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = NewsListAdapter(
                arrayListOf(),
                object :
                        NewsListAdapter.OnNewsItemClickListener {
                    override fun onClick(id: String) {
                        presenter.onItemClick(id)
                    }

                }
        )
        newsList.layoutManager =
                LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        newsList.adapter = adapter

        swipeRefresh.setOnRefreshListener { presenter.refreshNews() }
    }

    override fun showLoading() {
        swipeRefresh.isRefreshing = true
    }

    override fun hideLoading() {
        swipeRefresh.isRefreshing = false
    }

    override fun setData(data: List<NewsItem>) {
        adapter.setItems(data)
    }
}
