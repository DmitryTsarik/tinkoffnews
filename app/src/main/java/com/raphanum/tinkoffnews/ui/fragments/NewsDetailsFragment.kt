package com.raphanum.tinkoffnews.ui.fragments


import android.os.Bundle
import android.text.Html
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.raphanum.tinkoffnews.App
import com.raphanum.tinkoffnews.R
import com.raphanum.tinkoffnews.domain.models.NewsDetails
import com.raphanum.tinkoffnews.presentation.presenters.NewsDetailsPresenter
import com.raphanum.tinkoffnews.presentation.views.NewsDetailsView
import com.raphanum.tinkoffnews.utils.DateFormatUtils
import kotlinx.android.synthetic.main.fragment_news_details.*


class NewsDetailsFragment : MvpAppCompatFragment(),
        NewsDetailsView {
    @InjectPresenter
    lateinit var presenter: NewsDetailsPresenter

    @ProvidePresenter
    fun initPresenter() = App.appComponent.getNewsDetailsPresenter()

    override fun onCreateView(
        inflater: LayoutInflater?, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater!!.inflate(R.layout.fragment_news_details, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.getString("newsId")?.let { presenter.loadNews(it) }
    }

    companion object {
        fun newInstance(newsId: String): NewsDetailsFragment {
            val newsDetailsFragment = NewsDetailsFragment()
            val arguments = Bundle()
            arguments.putString("newsId", newsId)
            newsDetailsFragment.arguments = arguments
            return newsDetailsFragment
        }
    }

    override fun setData(data: NewsDetails) {
        title.text = data.title
        val htmlContent = Html.fromHtml(data.content)
        content.movementMethod = LinkMovementMethod.getInstance()
        content.text = htmlContent
        date.text = DateFormatUtils.getFormattedDate(data.publicationDate)
        stub.visibility = View.GONE
    }

    override fun showLoading() {
        progress.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progress.visibility = View.GONE
    }
}
