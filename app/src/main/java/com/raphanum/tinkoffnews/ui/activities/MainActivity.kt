package com.raphanum.tinkoffnews.ui.activities

import android.os.Bundle
import android.support.v4.app.Fragment
import android.widget.Toast
import com.raphanum.tinkoffnews.R
import com.raphanum.tinkoffnews.constants.Screens
import com.raphanum.tinkoffnews.ui.fragments.NewsDetailsFragment
import com.raphanum.tinkoffnews.ui.fragments.NewsListFragment
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.android.SupportFragmentNavigator
import ru.terrakok.cicerone.commands.Command
import ru.terrakok.cicerone.commands.Replace


class MainActivity : BaseActivity() {

    private val navigator: Navigator =
            object : SupportFragmentNavigator(supportFragmentManager, R.id.fragmentContainer) {
                override fun exit() {
                    finish()
                }

                override fun createFragment(screenKey: String?, data: Any?): Fragment? =
                    when (screenKey) {
                        Screens.NEWS_DETAILS_SCREEN -> {
                            if (data is String) {
                                NewsDetailsFragment.newInstance(
                                        data
                                )
                            } else null
                        }
                        else -> NewsListFragment()
                    }

                override fun showSystemMessage(message: String?) {
                    Toast.makeText(this@MainActivity, message, Toast.LENGTH_SHORT).show()
                }

            }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        navigatorHolder.setNavigator(navigator)

        if (savedInstanceState == null) {
            navigator.applyCommands(arrayOf<Command>(Replace(Screens.NEWS_LIST_SCREEN, null)))
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        navigatorHolder.removeNavigator()
    }
}
