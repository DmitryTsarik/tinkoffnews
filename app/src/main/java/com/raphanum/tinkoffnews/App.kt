package com.raphanum.tinkoffnews

import android.app.Application
import com.raphanum.tinkoffnews.di.AppComponent
import com.raphanum.tinkoffnews.di.AppModule
import com.raphanum.tinkoffnews.di.DaggerAppComponent

class App : Application() {
    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .build()
    }
}