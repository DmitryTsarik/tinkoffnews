package com.raphanum.tinkoffnews.presentation.views

import com.raphanum.tinkoffnews.domain.models.NewsItem

interface NewsListView : ViewWithLoader<List<NewsItem>>