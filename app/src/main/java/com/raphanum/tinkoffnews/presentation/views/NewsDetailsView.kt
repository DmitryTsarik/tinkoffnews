package com.raphanum.tinkoffnews.presentation.views

import com.raphanum.tinkoffnews.domain.models.NewsDetails

interface NewsDetailsView : ViewWithLoader<NewsDetails>