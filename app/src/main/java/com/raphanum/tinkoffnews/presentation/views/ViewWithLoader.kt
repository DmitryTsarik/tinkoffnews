package com.raphanum.tinkoffnews.presentation.views

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.SingleStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

interface ViewWithLoader<in DATA> : MvpView {
    @StateStrategyType(value = SingleStateStrategy::class)
    fun showLoading()

    @StateStrategyType(value = AddToEndSingleStrategy::class)
    fun hideLoading()

    @StateStrategyType(value = SingleStateStrategy::class)
    fun setData(data: DATA)
}