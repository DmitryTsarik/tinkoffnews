package com.raphanum.tinkoffnews.presentation.presenters

import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import com.raphanum.tinkoffnews.BuildConfig
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BasePresenter<VIEW : MvpView> : MvpPresenter<VIEW>() {
    private val compositeSubscription = CompositeDisposable()

    fun addSubscription(subscription: Disposable) {
        compositeSubscription.add(subscription)
    }

    override fun detachView(view: VIEW) {
        super.detachView(view)
        compositeSubscription.clear()
    }

    fun handleThrowable(throwable: Throwable) {
        // TODO: сделать нормальную человеческую обработку ошибок
        if (BuildConfig.DEBUG) {
            throwable.printStackTrace()
        }
    }
}