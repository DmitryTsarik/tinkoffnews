package com.raphanum.tinkoffnews.presentation.presenters

import com.arellomobile.mvp.InjectViewState
import com.raphanum.tinkoffnews.BuildConfig
import com.raphanum.tinkoffnews.constants.Screens
import com.raphanum.tinkoffnews.domain.NewsInteractor
import com.raphanum.tinkoffnews.presentation.views.NewsListView
import io.reactivex.android.schedulers.AndroidSchedulers
import ru.terrakok.cicerone.Router

@InjectViewState
class NewsListPresenter(private val newsInteractor: NewsInteractor, private val router: Router) :
        BasePresenter<NewsListView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        getNewsList()
    }

    fun getNewsList() {
        viewState.showLoading()
        addSubscription(
                newsInteractor.getNewsList()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            { items ->
                                run {
                                    viewState.hideLoading()
                                    viewState.setData(items)
                                }
                            },
                            { t: Throwable ->
                                viewState.hideLoading()
                                router.showSystemMessage(t.localizedMessage)
                                if (BuildConfig.DEBUG) {
                                    t.printStackTrace()
                                }
                            }
                    )
        )
    }

    fun refreshNews() {
        addSubscription(
                newsInteractor.refreshNewsList()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            { items ->
                                run {
                                    viewState.hideLoading()
                                    viewState.setData(items)
                                }
                            },
                            { t: Throwable ->
                                viewState.hideLoading()
                                router.showSystemMessage(t.localizedMessage)
                                if (BuildConfig.DEBUG) {
                                    t.printStackTrace()
                                }
                            }
                    )
        )
    }

    fun onItemClick(id: String) {
        router.navigateTo(Screens.NEWS_DETAILS_SCREEN, id)
    }
}