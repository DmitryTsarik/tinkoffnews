package com.raphanum.tinkoffnews.presentation.presenters

import com.arellomobile.mvp.InjectViewState
import com.raphanum.tinkoffnews.BuildConfig
import com.raphanum.tinkoffnews.domain.NewsInteractor
import com.raphanum.tinkoffnews.presentation.views.NewsDetailsView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.terrakok.cicerone.Router

@InjectViewState
class NewsDetailsPresenter(private val interactor: NewsInteractor, private val router: Router) :
        BasePresenter<NewsDetailsView>() {
    fun loadNews(id: String) {
        viewState.showLoading()
        addSubscription(
                interactor.getNewsDetails(id).subscribeOn(Schedulers.io()).observeOn(
                        AndroidSchedulers.mainThread()
                ).subscribe(
                        { newsDetails ->
                            viewState.setData(newsDetails)
                            viewState.hideLoading()
                        },
                        { t: Throwable ->
                            viewState.hideLoading()
                            router.exitWithMessage(t.localizedMessage)
                            if (BuildConfig.DEBUG) {
                                t.printStackTrace()
                            }
                        }
                )
        )
    }
}