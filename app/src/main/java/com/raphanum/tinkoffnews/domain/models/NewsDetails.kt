package com.raphanum.tinkoffnews.domain.models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
data class NewsDetails(
    @PrimaryKey val id: String,
    val title: String,
    val content: String,
    val publicationDate: Long
)