package com.raphanum.tinkoffnews.domain

import com.raphanum.tinkoffnews.domain.models.NewsDetails
import com.raphanum.tinkoffnews.domain.models.NewsItem
import io.reactivex.Single

interface NewsRepository {
    fun refreshNewsList(): Single<List<NewsItem>>

    fun getCachedNewsList(): Single<List<NewsItem>>

    fun getNewsDetails(id: String): Single<NewsDetails>
}