package com.raphanum.tinkoffnews.domain

class NewsInteractor(private val repository: NewsRepository) {
    fun refreshNewsList() = repository.refreshNewsList()

    fun getNewsList() = repository.getCachedNewsList()

    fun getNewsDetails(id: String) = repository.getNewsDetails(id)
}