package com.raphanum.tinkoffnews.domain.models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
data class NewsItem(
    @PrimaryKey val id: String,
    val title: String,
    val publicationDate: Long
)