package com.raphanum.tinkoffnews.data.repository

import com.raphanum.tinkoffnews.data.db.AppDatabase
import com.raphanum.tinkoffnews.domain.models.NewsDetails
import com.raphanum.tinkoffnews.domain.models.NewsItem
import io.reactivex.Single

class NewsLocalRepository(private val db: AppDatabase) {
    fun saveNewsList(newsList: List<NewsItem>) = db.newsItemDao().insert(newsList)

    fun getNewsList(): Single<List<NewsItem>> = db.newsItemDao().getNews()

    fun saveNewsDetails(newsDetails: NewsDetails) = db.newsDetailsDao().insert(newsDetails)

    fun getNewsDetails(id: String): Single<NewsDetails> = db.newsDetailsDao().getById(id)
}