package com.raphanum.tinkoffnews.data.api

import com.google.gson.GsonBuilder
import com.raphanum.tinkoffnews.constants.ApiConstants
import com.raphanum.tinkoffnews.BuildConfig
import com.raphanum.tinkoffnews.data.deserializers.NewsDetailsDeserializer
import com.raphanum.tinkoffnews.data.deserializers.NewsItemDeserializer
import com.raphanum.tinkoffnews.data.response.NewsResponse
import com.raphanum.tinkoffnews.domain.models.NewsDetails
import com.raphanum.tinkoffnews.domain.models.NewsItem
import io.reactivex.Single
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface TinkoffNewsApi {
    @GET(ApiConstants.GET_NEWS_LIST)
    fun getNewsList(): Single<NewsResponse<List<NewsItem>>>

    @GET(ApiConstants.GET_NEWS_DETAILS)
    fun getNewsDetails(@Query("id") id: String): Single<NewsResponse<NewsDetails>>

    companion object {
        fun create(): TinkoffNewsApi {
            val retrofit = Retrofit.Builder()
                .client(
                        OkHttpClient.Builder()
                            .addInterceptor(HttpLoggingInterceptor().setLevel(if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE))
                            .build()
                )
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(
                        GsonConverterFactory.create(
                                GsonBuilder()
                                    .registerTypeAdapter(
                                            NewsItem::class.java,
                                            NewsItemDeserializer()
                                    )
                                    .registerTypeAdapter(
                                            NewsDetails::class.java,
                                            NewsDetailsDeserializer()
                                    )
                                    .create()
                        )
                )
                .build()
            return retrofit.create(TinkoffNewsApi::class.java)
        }
    }
}