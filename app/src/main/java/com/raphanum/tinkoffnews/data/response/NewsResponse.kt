package com.raphanum.tinkoffnews.data.response

class NewsResponse<out T>(val payload: T)