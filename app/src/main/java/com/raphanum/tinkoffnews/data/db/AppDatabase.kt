package com.raphanum.tinkoffnews.data.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.raphanum.tinkoffnews.domain.models.NewsDetails
import com.raphanum.tinkoffnews.domain.models.NewsItem

@Database(entities = arrayOf(NewsItem::class, NewsDetails::class), version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun newsItemDao(): NewsItemDao
    abstract fun newsDetailsDao(): NewsDetailsDao
}