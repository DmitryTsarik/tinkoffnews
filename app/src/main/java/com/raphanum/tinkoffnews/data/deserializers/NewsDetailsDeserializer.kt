package com.raphanum.tinkoffnews.data.deserializers

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.raphanum.tinkoffnews.domain.models.NewsDetails
import java.lang.reflect.Type

class NewsDetailsDeserializer : JsonDeserializer<NewsDetails?> {

    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): NewsDetails? {
        val newsDetails: NewsDetails?
        json ?: return null
        val titleJsonObject = json.asJsonObject.get("title").asJsonObject
        val id = titleJsonObject.get("id").asString
        val title = titleJsonObject.get("text").asString
        val publicationDate = titleJsonObject.get("publicationDate").asJsonObject.get("milliseconds").asLong
        val content = json.asJsonObject.get("content").asString
        newsDetails = NewsDetails(
                id,
                title,
                content,
                publicationDate
        )
        return newsDetails
    }
}