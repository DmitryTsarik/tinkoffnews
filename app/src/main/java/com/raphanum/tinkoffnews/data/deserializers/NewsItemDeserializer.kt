package com.raphanum.tinkoffnews.data.deserializers

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.raphanum.tinkoffnews.domain.models.NewsItem
import java.lang.reflect.Type

class NewsItemDeserializer : JsonDeserializer<NewsItem?> {

    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): NewsItem? {
        var newsItem: NewsItem? = null
        json ?: return newsItem
        if (json.isJsonObject) {
            val newsItemObject = json.asJsonObject
            val id = newsItemObject.get("id").asString
            val title = newsItemObject.get("text").asString
            val publicationDate =
                    newsItemObject.get("publicationDate").asJsonObject.get("milliseconds").asLong

            newsItem = NewsItem(id, title, publicationDate)
        }
        return newsItem
    }
}