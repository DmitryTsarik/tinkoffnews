package com.raphanum.tinkoffnews.data.db

import android.arch.persistence.room.*
import com.raphanum.tinkoffnews.domain.models.NewsItem
import io.reactivex.Single

@Dao
interface NewsItemDao {
    @Query("SELECT * FROM newsItem")
    fun getNews(): Single<List<NewsItem>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(items: List<NewsItem>)

    @Update
    fun update(items: List<NewsItem>)
}