package com.raphanum.tinkoffnews.data.repository

import com.raphanum.tinkoffnews.data.api.TinkoffNewsApi
import com.raphanum.tinkoffnews.domain.models.NewsDetails
import com.raphanum.tinkoffnews.domain.models.NewsItem
import io.reactivex.Single

class NewsRestRepository(private val api: TinkoffNewsApi) {

    fun getNewsList(): Single<List<NewsItem>> {
        return api.getNewsList().map { response -> response.payload }
    }

    fun getNewsDetails(id: String): Single<NewsDetails> {
        return api.getNewsDetails(id).map { response -> response.payload }
    }
}