package com.raphanum.tinkoffnews.data.repository

import android.arch.persistence.room.EmptyResultSetException
import com.raphanum.tinkoffnews.domain.NewsRepository
import com.raphanum.tinkoffnews.domain.models.NewsDetails
import com.raphanum.tinkoffnews.domain.models.NewsItem
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class NewsRepositoryImpl(
    private val restRepository: NewsRestRepository,
    private val localRepository: NewsLocalRepository
) : NewsRepository {

    override fun refreshNewsList(): Single<List<NewsItem>> {
        return restRepository.getNewsList()
            .doOnSuccess { newsItems: List<NewsItem> ->
                localRepository.saveNewsList(
                    newsItems
                )
            }
            .subscribeOn(Schedulers.io())
    }

    override fun getCachedNewsList(): Single<List<NewsItem>> {
        return localRepository.getNewsList()
            .flatMap { items: List<NewsItem> ->
                if (items.isEmpty()) {
                    refreshNewsList()
                } else {
                    Single.just(items)
                }
            }
            .subscribeOn(Schedulers.io())
    }

    override fun getNewsDetails(id: String): Single<NewsDetails> {
        return localRepository.getNewsDetails(id)
            .onErrorResumeNext({ t: Throwable ->
                if (t is EmptyResultSetException) {
                    restRepository.getNewsDetails(id)
                        .doOnSuccess { details: NewsDetails ->
                            localRepository.saveNewsDetails(
                                details
                            )
                        }
                } else {
                    Single.error(t)
                }
            })
            .subscribeOn(Schedulers.io())
    }
}