package com.raphanum.tinkoffnews.data.db

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.raphanum.tinkoffnews.domain.models.NewsDetails
import io.reactivex.Single

@Dao
interface NewsDetailsDao {
    @Query("SELECT * FROM newsDetails WHERE id = :id")
    fun getById(id: String): Single<NewsDetails>

    @Insert
    fun insert(newsItem: NewsDetails)

    @Delete
    fun delete(newsItem: NewsDetails)
}